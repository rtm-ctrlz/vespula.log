# README #


This is a simple, flexible PSR-3 logging implementation for PHP.

Adapters include File, Email, Sql, None, Chromephp (console), PHPDebugBar, and ErrorLog (PHP's error_log() function)

Documentation is available at: https://vespula.bitbucket.io/log/

