<?php
declare(strict_types=1);

namespace Vespula\Log;

use Psr\Log\InvalidArgumentException;
use Vespula\Log\Adapter\AbstractAdapter;
use Psr\Log\AbstractLogger;
use Psr\Log\LogLevel;
use Vespula\Log\Exception\InvalidMethodException;
use function array_keys;
use function array_search;
use function in_array;
use function is_a;
use function is_scalar;
use function print_r;

/**
 * This class enables very simple PSR-3 compliant logging
 *
 * <code>
 * // Screen adapter echos out log on screen
 * $adapter = new \Vespula\Log\Adapter\Screen;
 * $logger = new \Vespula\Log\Log($adapter);
 *
 * // There are 7 log levels
 * $logger->alert('This is an alert');
 * $logger->notice('This is a notice');
 *
 * // Use context for replacements
 * $context = [
 *     'name'=>'Joe User'
 * ];
 * $logger->info('User {name} logged in', $context);
 *
 * // Use the catch-all log method
 *
 * $logger->log(\Psr\Log\LogLevel::CRITICAL, 'This is critical!!!');
 * </code>
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
class Log extends AbstractLogger
{

    /**
     * The logging adapter(s)
     *
     * @var array<string, AbstractAdapter>
     */
    protected $adapters = [];

    /**
     * Log levels in order of precedence. DEBUG is least urgent
     *
     * @var string[] The log levels from Psr\Log\LogLevel
     * @todo Look at ensuring these stay up to date with Psr/Log/LogLevel
     */
    protected $levels = [
        LogLevel::DEBUG,
        LogLevel::INFO,
        LogLevel::NOTICE,
        LogLevel::WARNING,
        LogLevel::ERROR,
        LogLevel::CRITICAL,
        LogLevel::ALERT,
        LogLevel::EMERGENCY,
    ];

    /**
     * @param string $key
     * @param AbstractAdapter $adapter
     */
    public function __construct(string $key, AbstractAdapter $adapter)
    {
        $this->adapters[$key] = $adapter;
    }

    /**
     * Add an adapter to the adapter list
     *
     * @param string $key
     * @param AbstractAdapter $adapter
     * @return void
     */
    public function addAdapter(string $key, AbstractAdapter $adapter)
    {
        $this->adapters[$key] = $adapter;
    }

    /**
     * Get the adapters
     *
     * @return array<string, AbstractAdapter> Adapters
     */
    public function getAdapters(): array
    {
        return $this->adapters;
    }

    /**
     * Get an adapter by key
     *
     * @param string $key
     * @return AbstractAdapter|false False if no adapter matches key, or the adapter object
     */
    public function getAdapter(string $key = null)
    {

        if (! $key) {
            $keys = array_keys($this->adapters);
            if ($keys) {
                $key = $keys[0];
            }
        }

        if (isset($this->adapters[$key])) {
            return $this->adapters[$key];
        }
        return false;
    }

    /**
     * Writes a message to the log, calling the appropriate method based on
     * level
     *
     * @param string $level Must be one of the PsrLoglevel levels
     * @param string $message
     * @param array<int|string, mixed> $context
     * @return void
     * @throws InvalidArgumentException
     */
    public function log($level, $message, array $context = [])
    {
        if (!in_array($level, $this->levels)) {
            throw new InvalidArgumentException('Invalid log level ' . $level);
        }

        foreach ($this->adapters as $adapter) {
            $level_key = array_search($level, $this->levels);
            if (is_a($adapter, AbstractAdapter::class)) {
                $min_key = array_search($adapter->getMinLevel(), $this->levels);
                // If the log level is less than the min threshold, then don't log.
                if ($level_key < $min_key) {
                    continue;
                }
            }

            $message = $this->processContext($message, $context);
            $adapter->write($level, $message);
        }

    }

    /**
     * Replaces context placeholders with the corresponding value
     *
     * @param string $message
     * @param array<int|string, mixed> $context
     * @return string
     */
    protected function processContext($message, array $context = [])
    {
        $replace_pairs = array();
        foreach ($context as $key=>$value) {
            $replace_pairs['{' . $key . '}'] = $this->fixContextValue($value);
        }
        return strtr($message, $replace_pairs);
    }

    /**
     * Checks if a context value is a string or array. If array, uses print_r
     *
     * @param mixed $value
     * @return string
     */
    protected function fixContextValue($value): string
    {
        if (! is_scalar($value)) {
            return print_r($value, true);
        }

        return (string)$value;
    }

    /**
     * @param string $method
     * @param array<mixed> $params
     * @return void
     * @throws InvalidMethodException
     */
    public function __call(string $method, array $params)
    {
        throw new InvalidMethodException(
            'Invalid log type ' . $method
        );
    }
}
