<?php
namespace Vespula\Log\Exception;

use Vespula\Log\Exception;

class InvalidColumnMapException extends Exception 
{
}
