<?php
namespace Vespula\Log\Exception;

use Vespula\Log\Exception;

class InvalidArgumentException extends Exception 
{
}
