<?php
declare(strict_types=1);

namespace Vespula\Log\Adapter;

use Vespula\Log\Exception\InvalidArgumentException;
use function array_unshift;
use function date;
use function error_log;
use function file_put_contents;
use function implode;
use function in_array;
use function rtrim;
use function str_repeat;
use const PHP_EOL;

/**
 * Log adapter for logging to email
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 */

class Email extends AbstractAdapter
{
    /**
     * The error_log message type
     */
    const MESSAGE_TYPE = 1;

    /**
     *
     * @var string email to
     */
    protected $to;

    /**
     *
     * @var string Email from
     */
    protected $from;

    /**
     *
     * @var string Email subject
     */
    protected $subject;

    /**
     *
     * @var string[] Additional headers for mail
     */
    protected $headers = [];

    /**
     *
     * @var bool Append the log level to the subject (default is true)
     */
    protected $append_level = true;

    /**
     * Output to this file instead of mail
     *
     * @var string|false false for normal mail output or a path
     */
    protected $path = false;

    /**
     * @var string The delimiter between mail headers
     */
    protected $header_delimiter = "\r\n";

    /**
     * Email constructor.
     *
     * @param string $to
     * @param string $from
     * @param string $subject
     * @param string[] $headers
     */
    public function __construct(string $to, string $from, string $subject = 'Application Log', array $headers = [])
    {
        $this->to = $to;
        $this->from = $from;
        $this->subject = $subject;
        $this->headers = $headers;
    }

    /**
     * Append the log level to the subject
     *
     * @return void
     */
    public function appendLevelOn()
    {
        $this->append_level = true;
    }

    /**
     * DO NOT append the log level to the subject
     *
     * @return void
     */
    public function appendLevelOff()
    {
        $this->append_level = false;
    }

    /**
     * Set the mail header delimiter
     *
     * @param string $delimiter
     * @return void
     * @throws InvalidArgumentException
     */
    public function setHeaderDelimiter($delimiter)
    {
        $valid = [
            "\r\n",
            "\n"
        ];

        if (!in_array($delimiter, $valid)) {
            throw new InvalidArgumentException('Invalid delimiter. Must be `\r\n` or `\n`');
        }

        $this->header_delimiter = $delimiter;
    }
    /**
     * Set capture to true. Rather than sending mail, the adapter returns an array.
     * This is used for testing.
     *
     * @param string|false $path A path for email files, or false to disable
     * @return void
     */
    public function setPath($path = '/tmp')
    {
        $this->path = $path;
    }


    /**
     * Write the log message
     *
     * @param string $level
     * @param string $message
     * @return bool
     */
    public function write(string $level, string $message)
    {
        $timestamp = $this->getTimestamp();
        $message = $this->buildMessage($level, $message, $timestamp);

        $from = 'From: ' . $this->from;
        $subject = 'Subject: ' . $this->subject;

        if ($this->append_level) {
            $subject .= ' (' . $level . ')';
        }

        array_unshift($this->headers, $subject);
        array_unshift($this->headers, $from);

        $headers = implode($this->header_delimiter, $this->headers);

        // If capture is set to true, return data as an array (use for testing)
        if ($this->path !== false) {
            $output = [
                'to'=>$this->to,
                'message'=>$message,
                'subject'=>$subject,
                'headers'=>$headers
            ];
            return $this->writeToFile($output);
        }

        return error_log($message, self::MESSAGE_TYPE , $this->to, $headers);

    }

    /**
     * @param array<string, string> $output
     * @return bool
     *
     * @phpstan-param array{to: string, headers: string, message: string} $output
     */
    protected function writeToFile(array $output)
    {

        $text = 'To: ' . $output['to'] . PHP_EOL;
        $text .= 'Sent: ' . date('c') . PHP_EOL;
        $text .= $output['headers'] . PHP_EOL;
        $text .= str_repeat('-', 80) . PHP_EOL;
        $text .= $output['message'] . PHP_EOL;
        $text .= str_repeat('=', 80) . PHP_EOL . PHP_EOL;

        $file = rtrim((string)$this->path, '/') . '/vespula_log' . date('Ymd\THis') . '.txt';
        return file_put_contents($file, $text) > 0;
    }
}
