<?php
declare(strict_types=1);

namespace Vespula\Log\Adapter;

use Exception;
use Vespula\Log\Exception\InvalidArgumentException;
use function error_log;
use function in_array;
use function is_null;
use const PHP_EOL;

/**
 * This adapter uses the php function `error_log()` to output the log
 *
 * This is used for writing to a file, email, or system/sapi logs
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 */
class ErrorLog extends AbstractAdapter
{

    /**
     * Use the PHP system logger (default)
     */
    const TYPE_PHP_LOG = 0;

    /**
     * Use the Email logging
     */
    const TYPE_EMAIL = 1;

    /**
     * Use the File logging
     */
    const TYPE_FILE = 3;

    /**
     * Use the SAPI log handler
     */
    const TYPE_SAPI_LOG = 4;
    /**
     * The message type for error_log()
     *
     * @var int
     */
    protected $message_type;

    /**
     * @var string|null Email or filename, depending on message type
     */
    protected $destination;

    /**
     *
     * @var string|null Additional headers used for the `mail()` function
     */
    protected $headers;

    const VALID_TYPES = [
        self::TYPE_PHP_LOG,
        self::TYPE_EMAIL,
        self::TYPE_FILE,
        self::TYPE_SAPI_LOG
    ];

    /**
     * Constructor
     *
     * @param int $message_type
     * @param string $destination
     * @param string $headers
     * @throws InvalidArgumentException
     */
    public function __construct(int $message_type = 0, string $destination = null, string $headers = null)
    {
        $this->message_type = $message_type;

        if (!in_array($this->message_type, static::VALID_TYPES)) {
            throw new InvalidArgumentException('Invalid message type: ' . $message_type);
        }

        $this->destination = $destination;
        $this->headers = $headers;
    }

    /**
     * Write log data based on message type
     *
     * @param string $level
     * @param string $message
     * @return bool
     */
    public function write(string $level, string $message)
    {
        $timestamp = $this->getTimestamp();
        $message = $this->buildMessage($level, $message, $timestamp);
        return error_log(
            $message . (is_null($this->destination) ? '' : PHP_EOL),
            $this->message_type,
            $this->destination ?? '',
            $this->headers ?? ''
        );
    }

}
