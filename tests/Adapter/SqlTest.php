<?php
namespace Vespula\Log\Adapter;
use Vespula\Log\Adapter\Sql;
use PDO;
use Psr\Log\LogLevel;
use PHPUnit\Framework\TestCase;

class SqlTest extends TestCase 
{
    
    protected $pdo;
    protected $adapter_log;
    protected $adapter_logging;
    protected $adapter_error;
    
    public function setUp(): void
    {
        $this->pdo = new PDO('sqlite::memory:');
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->createDatabase();
        
        $this->adapter_log = new Sql($this->pdo, 'log');
        $this->adapter_logging = new Sql($this->pdo, 'logging');
        $this->adapter_error = new Sql($this->pdo, 'foo');
    }
    
    protected function createDatabase()
    {
        $sql_log = 'CREATE TABLE log ('
                . 'level VARCHAR(50), '
                . 'message TEXT, '
                . 'timestamp TEXT'
                . ')';
        
        $this->pdo->query($sql_log);
        
        $sql_logging = 'CREATE TABLE logging ('
                . 'type VARCHAR(50), '
                . 'msg TEXT, '
                . 'datetime TEXT'
                . ')';
        
        $this->pdo->query($sql_logging);     
    }
    
    public function testWrite()
    {
        $level = LogLevel::DEBUG;
        $message = 'Here is a message';
        $timestamp = date('Y-m-d H:i:s');
        $this->adapter_log->write($level, $message);
        
        $select = 'SELECT * FROM log';
        $stm = $this->pdo->prepare($select);
        $stm->execute();
        
        $row = $stm->fetch(PDO::FETCH_ASSOC);
        
        $expected  = [
            'level'=>'debug',
            'message'=>$message,
            'timestamp'=>$timestamp
        ];
        
        $this->assertEquals($expected, $row);
        
    }
    
    public function testSetCols()
    {
        $level = LogLevel::DEBUG;
        $message = 'Here is a message';
        $timestamp = date('Y-m-d H:i:s');
        $this->adapter_logging->setCols([
            'level'=>'type',
            'message'=>'msg',
            'timestamp'=>'datetime'
        ]);
        $this->adapter_logging->write($level, $message);
        
        $select = 'SELECT * FROM logging';
        $stm = $this->pdo->prepare($select);
        $stm->execute();
        
        $row = $stm->fetch(PDO::FETCH_ASSOC);
        
        $expected  = [
            'type'=>'debug',
            'msg'=>$message,
            'datetime'=>$timestamp
        ];
        
        $this->assertEquals($expected, $row);
        
    }
    
    public function testMissingLevelCol()
    {
        $this->expectException(\Vespula\Log\Exception\InvalidColumnMapException::class);

        $this->adapter_log->setCols([
            'blah'=>'type',
            'message'=>'msg',
            'timestamp'=>'datetime'
        ]);
    }
    
    public function testMissingMessageCol()
    {
        $this->expectException(\Vespula\Log\Exception\InvalidColumnMapException::class);

        $this->adapter_log->setCols([
            'level'=>'type',
            'msg'=>'msg',
            'timestamp'=>'datetime'
        ]);
    }
    
    public function testMissingTimestampCol()
    {
        $this->expectException(\Vespula\Log\Exception\InvalidColumnMapException::class);

        $this->adapter_log->setCols([
            'level'=>'type',
            'message'=>'msg',
            'ts'=>'datetime'
        ]);
    }

    public function testWriteError()
    {
        $this->expectException(\Exception::class);

        $level = LogLevel::DEBUG;
        $message = 'Here is a message';
        $timestamp = date('c');

        $this->adapter_log->setCols([
            'level'=>'type',
            'message'=>'msg',
            'timestamp'=>'foo'
        ]);

        $this->adapter_log->write($level, $message);

    }

    public function testWriteErrorAdapter()
    {
        $this->expectException(\Exception::class);

        $level = LogLevel::DEBUG;
        $message = 'Here is a message';
        

        $this->adapter_error->write($level, $message);

    }

    public function testGetCount()
    {
        $level = LogLevel::DEBUG;
        $message = 'Here is a message';
        for ($i=0; $i<10; $i++) {
            $this->adapter_log->write($level, $message);
        }

        $count = $this->adapter_log->getCount();
        $expected = 10;

        $this->assertEquals($expected, $count);
    }

    public function testGetEntries()
    {
        $level = LogLevel::DEBUG;
        $timestamp = date('Y-m-d H:i:s');
        $message = 'Here is a message';
        for ($i=0; $i<10; $i++) {
            $this->adapter_log->write($level, $message);
        }

        $entries = $this->adapter_log->getEntries();

        $expected = [];
        for ($i=0; $i<10; $i++) {
            $entry = [
                'timestamp'=>$timestamp,
                'level'=>$level,
                'message'=>$message
            ];
            array_push($expected, $entry);
        }

        $this->assertEquals($expected, $entries);
    }

    public function testGetEntriesLimitOffset()
    {
        $level_debug = LogLevel::DEBUG;
        $level_info = LogLevel::INFO;
        $timestamp = date('Y-m-d H:i:s');
        $message = 'Here is a message';
        $level = $level_debug;
        for ($i=0; $i<10; $i++) {
            if ($i > 2) {
                $level = $level_info;
            }
            $this->adapter_log->write($level, $message);
        }

        $entries = $this->adapter_log->getEntries(2);

        $expected = [];
        for ($i=0; $i<2; $i++) {
            $entry = [
                'timestamp'=>$timestamp,
                'level'=>$level_debug,
                'message'=>$message
            ];
            array_push($expected, $entry);
        }

        $this->assertEquals($expected, $entries);



        $entries = $this->adapter_log->getEntries(2, 5);


        $expected = [];

        for ($i=0; $i<2; $i++) {
            $entry = [
                'timestamp'=>$timestamp,
                'level'=>$level_info,
                'message'=>$message
            ];
            array_push($expected, $entry);
        }

        $this->assertEquals($expected, $entries);


    }

    public function testGetEntriesOrderBy()
    {
        $level = LogLevel::INFO;
        
        $message = 'Here is a message';

        for ($i=0; $i<10; $i++) {
            $sql = "INSERT INTO log values(?,?,?)";
            $stmt = $this->pdo->prepare($sql);
            $date = new \DateTime('2020-01-01 00:00:0' . $i . '+00:00');
            $params = [
                $level,
                $message, 
                $date->format('c')
            ];
            $stmt->execute($params);
        }

        $entries = $this->adapter_log->getEntries(2, 0, ['rowid DESC']);

        $expected = [];
        
        $entry_one = [
            'timestamp'=>'2020-01-01T00:00:09+00:00',
            'level'=>$level,
            'message'=>$message
        ];
        $entry_two = [
            'timestamp'=>'2020-01-01T00:00:08+00:00',
            'level'=>$level,
            'message'=>$message
        ];
        array_push($expected, $entry_one);
        array_push($expected, $entry_two);
        

        $this->assertEquals($expected, $entries);


    }
}
