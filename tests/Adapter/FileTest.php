<?php
namespace Vespula\Log\Adapter;


use PHPUnit\Framework\TestCase;
class FileTest extends TestCase {
    
    protected $filename = '/tmp/vespula_log.txt';


    public function setUp(): void 
    {
        if (file_exists($this->filename)) {
            unlink($this->filename);
        }
    }
    
    public function testWriteFile()
    {
        
        $adapter = new \Vespula\Log\Adapter\File($this->filename);
        
        $level = 'info';
        $message = 'testing a write!';
        $timestamp = date('c');
        $adapter->write($level, $message);
        
        $level = 'INFO';
        $expected = "[$timestamp]\t[$level]\t$message" . PHP_EOL;
        $actual = file_get_contents($this->filename);
        $this->assertEquals($expected, $actual);
    }
    
    public function testBadPath()
    {
        $this->expectException(\Vespula\Log\Exception\InvalidArgumentException::class);
        $adapter = new \Vespula\Log\Adapter\File('/root/log.txt');
        // This should not be written. Exception should be thrown first.
        $adapter->write('info', 'some message');
    }

    public function testGetEntriesLimit()
    {
        $contents = '';

        $levels = [
            'INFO', 
            'ERROR',
            'DEBUG',
            'WARNING',
            'NOTICE'
        ];

        $message = 'Here is a test log message';

        for ($x=0; $x<50; $x++) {
            
            $key = $x%count($levels);
            $level = $levels[$key];
        
            $date = new \DateTime("2020-01-01T00:00:$x");
            $date_string = $date->format('c');
            
            $contents .= "[$date_string]\t[$level]\t$message" . PHP_EOL;
        }

        file_put_contents($this->filename, $contents);

        $adapter = new \Vespula\Log\Adapter\File($this->filename);

        $entries = $adapter->getEntries(5);
        $expected = [];
        
        for ($z=45; $z<50; $z++) {
            $exp_date = new \DateTime("2020-01-01T00:00:$z");
            $date_string = $exp_date->format('c');

            $key = $z%count($levels);
            $level = $levels[$key];
            $entry = [
                'timestamp'=>"[$date_string]",
                'level'=>"[$level]",
                'message'=>$message
            ];
            array_push($expected, $entry);
        }
        $expected = array_reverse($expected);

        $this->assertEquals($expected, $entries);

        // Now with offset
        $entries_paged = $adapter->getEntries(5, 2);
        $expected_paged = [];
        
        for ($z=40; $z<45; $z++) {
            $exp_date = new \DateTime("2020-01-01T00:00:$z");
            $date_string = $exp_date->format('c');

            $key = $z%count($levels);
            $level = $levels[$key];
            $entry = [
                'timestamp'=>"[$date_string]",
                'level'=>"[$level]",
                'message'=>$message
            ];
            array_push($expected_paged, $entry);
        }
        $expected_paged = array_reverse($expected_paged);

        $this->assertEquals($expected_paged, $entries_paged);
            

    }

    public function testGetEntriesMultiline()
    {
        $message_one = 'This is a message';
        $message_two = 'This is a message' . PHP_EOL . 'This is a message';

        $timestamp = date('c');

        $adapter = new \Vespula\Log\Adapter\File($this->filename);
        $adapter->write(\PSR\Log\LogLevel::INFO, $message_one);
        $adapter->write(\PSR\Log\LogLevel::INFO, $message_two);
        $adapter->write(\PSR\Log\LogLevel::INFO, $message_one);

        $entries = $adapter->getEntries(3);
        $expected = [];
        $info = strtoupper(\PSR\Log\LogLevel::INFO);
        
        $entry_one = [
            'timestamp'=>"[$timestamp]",
            'level'=>"[$info]",
            'message'=>$message_one
        ];

        $entry_two = [
            'timestamp'=>"[$timestamp]",
            'level'=>"[$info]",
            'message'=>$message_two
        ];

        

        array_push($expected, $entry_one);
        array_push($expected, $entry_two);
        array_push($expected, $entry_one);
        
        $expected = array_reverse($expected);

        $this->assertEquals($expected, $entries);
            

    }
    
    public function testGetEntriesLifo()
    {
        $message_one = 'This is the first message';
        $message_two = 'This is the second message';
        $message_three = 'This is the third message';

        $timestamp = date('c');

        $adapter = new \Vespula\Log\Adapter\File($this->filename);
        $adapter->write(\PSR\Log\LogLevel::INFO, $message_one);
        $adapter->write(\PSR\Log\LogLevel::INFO, $message_two);
        $adapter->write(\PSR\Log\LogLevel::INFO, $message_three);
        $lifo = false;
        $entries = $adapter->getEntries(3, 0, $lifo);
        $expected = [];
        $info = strtoupper(\PSR\Log\LogLevel::INFO);
        
        $entry_one = [
            'timestamp'=>"[$timestamp]",
            'level'=>"[$info]",
            'message'=>$message_one
        ];

        $entry_two = [
            'timestamp'=>"[$timestamp]",
            'level'=>"[$info]",
            'message'=>$message_two
        ];

        $entry_three = [
            'timestamp'=>"[$timestamp]",
            'level'=>"[$info]",
            'message'=>$message_three
        ];

        

        array_push($expected, $entry_one);
        array_push($expected, $entry_two);
        array_push($expected, $entry_three);

        $this->assertEquals($expected, $entries);
            

    }

    public function testGetCount()
    {
        $message = 'This is a message';


        $adapter = new \Vespula\Log\Adapter\File($this->filename);

        for ($i=0; $i<10; $i++) {
            $adapter->write(\PSR\Log\LogLevel::INFO, $message);
        }
        
        

        $count= $adapter->getCount();
        $expected = 10;
        

        $this->assertEquals($expected, $count);
            

    }

    public function testGetEntriesStartPattern()
    {
        $message_one = 'This is the first message';
        $message_two = 'This is the second message';
        $message_three = 'This is the third message';

        $timestamp = date('Ymd :: H:i:s');

        $adapter = new \Vespula\Log\Adapter\File($this->filename);
        $adapter->setDateFormat('Ymd :: H:i:s');
        $year = date('Y');
        $month = date('m');
        $day = date('d');
        $pattern = '/^\[' . $year.$month.$day . ' :: [0-9]{2}:[0-9]{2}:[0-9]{2}\]/';

        $adapter->setLineStartPattern($pattern);
        $adapter->write(\PSR\Log\LogLevel::INFO, $message_one);
        $adapter->write(\PSR\Log\LogLevel::INFO, $message_two);
        $adapter->write(\PSR\Log\LogLevel::INFO, $message_three);
        $entries = $adapter->getEntries(3);
        $expected = [];
        $info = strtoupper(\PSR\Log\LogLevel::INFO);
        
        $entry_one = [
            'timestamp'=>"[$timestamp]",
            'level'=>"[$info]",
            'message'=>$message_one
        ];

        $entry_two = [
            'timestamp'=>"[$timestamp]",
            'level'=>"[$info]",
            'message'=>$message_two
        ];

        $entry_three = [
            'timestamp'=>"[$timestamp]",
            'level'=>"[$info]",
            'message'=>$message_three
        ];

        

        array_push($expected, $entry_one);
        array_push($expected, $entry_two);
        array_push($expected, $entry_three);
        
        $expected = array_reverse($expected);

        $this->assertEquals($expected, $entries);
            

    }

    public function testBadStartPattern()
    {
        $this->expectException(\Vespula\Log\Exception::class);
        $message_one = 'This is the first message';

        $adapter = new \Vespula\Log\Adapter\File($this->filename);

        $pattern = '/^badpattern/';

        $adapter->setLineStartPattern($pattern);
        $adapter->write(\PSR\Log\LogLevel::INFO, $message_one);
        $adapter->getEntries(3);
        
    }

}

