<?php
namespace Vespula\Log\Adapter;
use Psr\Log\LogLevel;
use PHPUnit\Framework\TestCase;
class ChromeTest extends TestCase 
{
    
    
    public function testWrite()
    {
        /*
        $stub = $this->getMockBuilder(\Vespula\Log\Adapter\Chrome::class)
                     ->getMock();
        */
        $stub = $this->createMock(\Vespula\Log\Adapter\Chrome::class);


        $timestamp = date('c');
        $level = 'INFO';
        $message = 'This is a test';
        $expected = "[$timestamp]\t[$level]\t$message" . PHP_EOL;
        $stub->method('write')->willReturn($expected);
        
        $this->assertEquals($expected, $stub->write($level, $message));
        
    }
    
    public function testSetLoglevelMap()
    {
        $map = [
            LogLevel::DEBUG=>'info',
            LogLevel::INFO=>'info',
            LogLevel::NOTICE=>'warn',
            LogLevel::WARNING=>'warn',
            LogLevel::ERROR=>'error',
            LogLevel::CRITICAL=>'error',
            LogLevel::ALERT=>'error',
            LogLevel::EMERGENCY=>'error'
        ];
        $adapter = new \Vespula\Log\Adapter\Chrome;
        $adapter->setLoglevelMap($map);
        
        $this->assertEquals($map, $adapter->getLoglevelMap());
        
        $map = [
            LogLevel::DEBUG=>'warn',
            LogLevel::INFO=>'info',
            LogLevel::NOTICE=>'info',
            LogLevel::WARNING=>'info',
            LogLevel::EMERGENCY=>'info',
        ];
        
        $expected = [
            LogLevel::DEBUG=>'warn',
            LogLevel::INFO=>'info',
            LogLevel::NOTICE=>'info',
            LogLevel::WARNING=>'info',
            LogLevel::ERROR=>'error',
            LogLevel::CRITICAL=>'error',
            LogLevel::ALERT=>'error',
            LogLevel::EMERGENCY=>'info'
        ];
        
        $adapter->setLoglevelMap($map);
        
        $this->assertEquals($expected, $adapter->getLoglevelMap());
    }

}